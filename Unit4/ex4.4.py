import datetime


def gen_secs():
    return (second for second in range(60))


def gen_minutes():
    return (minute for minute in range(60))


def gen_hours():
    return (hour for hour in range(23))


def gen_time():
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                yield datetime.time(hour, minute, second)


def gen_years(start = 2023):
    while True:
        yield start
        start += 1


def gen_months():
    return (month for month in range(1, 13))


def gen_days(month, leap_year):
    days_in_month = {
        1: 31,
        2: 29 if leap_year else 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31
    }
    return (day for day in range(1, days_in_month[month] + 1))


def gen_date():
    for year in gen_years(2019):
        for month in gen_months():
            for day in gen_days(month, year % 4 == 0):
                for time in gen_time():
                    date_str = "%02d/%02d/%04d %s" % (day, month, year, time)
                    yield date_str


def main():
    date_gen = gen_date()
    i = 1
    while True:
        date = next(date_gen)
        if i % 1000000 == 0:
            print(date)
        i += 1

main()
