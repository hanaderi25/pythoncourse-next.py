def parse_ranges(ranges_string):
    range_strings = ranges_string.split(',')
    range_generator = (range_string.split('-') for range_string in range_strings)
    number_generator = (number for start, stop in range_generator for number in range(int(start), int(stop) + 1))
    return number_generator


print(list(parse_ranges("1-2,4-4,8-10")))
print(list(parse_ranges("0-0,4-8,20-21,43-45")))