def get_fibo():
    num1 = 0
    num2 = 1
    while True:
        result = num1 + num2
        yield result
        num1 = num2
        num2 = result

        
def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
main()
