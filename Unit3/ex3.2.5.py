def read_file(file_name):
    try:
        string = "__CONTENT_START__\n"
        with open(file_name, 'r') as file:
            content = file.read()
            string += content
    except FileNotFoundError:
        string += "__NO_SUCH_FILE__\n"
    else:
        print("The file exists")
    finally:
        string += "__NO_SUCH_FILE__\n"
        return string


def main():
    print(read_file("file"))


main()
