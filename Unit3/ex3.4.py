import string


class UsernameContainsIllegalCharacter(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Your username contains an illegal character"

    def get_arg(self):
        return self._arg


class UsernameTooShort(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Your username is too short"

    def get_arg(self):
        return self._arg


class UsernameTooLong(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Your username is too long"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacter(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "The password is missing a character"

    def get_arg(self):
        return self._arg


class PasswordMissingUppercase(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return PasswordMissingCharacter.__str__(self) + "(Uppercase)"


class PasswordMissingLowercase(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return PasswordMissingCharacter.__str__(self) + "(Lowercase)"


class PasswordMissingDigit(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return PasswordMissingCharacter.__str__(self) + "(Digit)"


class PasswordMissingSpecial(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return PasswordMissingCharacter.__str__(self) + "(Special)"


class PasswordTooShort(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "The password is too short"

    def get_arg(self):
        return self._arg


class PasswordTooLong(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "The password is too long"

    def get_arg(self):
        return self._arg


def check_input(username, password):
    try:
        for character in username:
            if character.isalnum() == False and character != '_':
                raise UsernameContainsIllegalCharacter(username)
        if len(username) < 3:
            raise UsernameTooShort(username)
        if len(username) > 16:
            raise UsernameTooLong(username)
        if not any(c.isupper() for c in password):
            raise PasswordMissingUppercase(password)
        if not any(c.islower() for c in password):
            raise PasswordMissingLowercase(password)
        if not any(c.isdigit() for c in password):
            raise PasswordMissingDigit(password)
        if not any(c in string.punctuation for c in password):
            raise PasswordMissingSpecial(password)
        if len(password) < 8:
            raise PasswordTooShort(password)
        if len(password) > 40:
            raise PasswordTooLong(password)
    except UsernameContainsIllegalCharacter as e:
        print(e)
    except UsernameTooShort as e:
        print(e)
    except UsernameTooLong as e:
        print(e)
    except PasswordMissingUppercase as e:
        print(e)
    except PasswordMissingLowercase as e:
        print(e)
    except PasswordMissingDigit as e:
        print(e)
    except PasswordMissingSpecial as e:
        print(e)
    except PasswordTooShort as e:
        print(e)
    except PasswordTooLong as e:
        print(e)


def main():
    check_input("1", "2")
    check_input("0123456789ABCDEFG", "2")
    check_input("A_a1.", "12345678")
    check_input("A_1", "2")
    check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")
    check_input("A_1", "4BCD3F6.1jk1mn0p")


main()
