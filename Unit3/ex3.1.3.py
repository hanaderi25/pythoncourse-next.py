def stop_iteration_func():
    my_list = ['a', 'b', 'c', 'd']
    my_iterator = iter(my_list)
    while True:
        print(next(my_iterator))


def zero_division_error_func():
    num1 = 8
    num2 = 0
    result = num1/num2
    print(result)


def assertion_error_func():
    num1 = 1
    num2 = 2
    assert num1 < num2, "num2 is not bigger than num1"


def import_error_func():
    import not_a_real_moudle


def key_error_func():
    my_dict = {'Hana': 160, 'Lea': 159, 'Yelena': 165, 'Amnon': 175}
    value = my_dict['Galina']
    print(value)


def syntax_error_func():
    my_name = 'Han'
    if my_name == 'Hana':
        print("WOW")
    esle:
        print("Nooooo")


def indentation_error_func():
    my_name = 'Hana'
    if my_name == 'Hana':
    print("WOW")


def type_error_func():
    num = 1
    letter = 'h'
    result = num + letter
    print(result)


def main():
    stop_iteration_func()
    zero_division_error_func()
    assertion_error_func()
    import_error_func()
    key_error_func()
    syntax_error_func()
    indentation_error_func()
    type_error_func()

main()

