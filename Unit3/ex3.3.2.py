class UnderAgeError(Exception):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "You are under 18 years old. Your age is {}. You can come to Ido party in {} years.".format(self._arg,
                                                                                                           18 - self._arg)

    def get_arg(self):
        return self._arg


def send_invitation(name, age):
    try:
        if int(age) < 18:
            raise UnderAgeError(age)
        else:
            print("You should send an invite to " + name)
    except UnderAgeError as e:
        print(e)


def main():
    send_invitation("Mika", 20)
    send_invitation("Yeva", 17)


main()
