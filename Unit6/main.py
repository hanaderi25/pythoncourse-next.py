from file2 import BirthDayCard, GreetingCard


def main():
    birthday_card = BirthDayCard()
    greeting_card = GreetingCard()
    birthday_card.greeting_msg()
    greeting_card.greeting_msg()

if __name__ == '__main__':
    main()