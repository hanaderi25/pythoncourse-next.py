import pyttsx3


engine = pyttsx3.init()
engine.setProperty('rate', 150)
engine.setProperty('volume', 1.0)
sentence = "First time I'm using a package in the Next.py course"
engine.say(sentence)
engine.runAndWait()
