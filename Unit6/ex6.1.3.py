import tkinter as tk


def show_picture():
    pic = tk.PhotoImage(file="yellow.png")
    image_label = tk.Label(image=pic)
    image_label.config(image=pic)
    image_label.image = pic

    image_label.pack()


def main():
    root = tk.Tk()

    w = tk.Label(root, text="What is my favorite color?")
    b = tk.Button(root, text="click to find out!", command=show_picture)
    w.pack()
    b.pack()
    root.mainloop()


main()
