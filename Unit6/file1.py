class GreetingCard:

    def __init__(self, sender="Eyal Ch", recipient="Dana Ev"):
        self._sender = sender
        self._recipient = recipient


    def greeting_msg(self):
        print("The sender: %s, the recipient: %s" % (self._sender, self._recipient))