from file1 import GreetingCard


class BirthDayCard(GreetingCard):

    def __init___(self, age=0, recipient="Dana Ev", sender="Eyal Ch"):
        super().__init__(recipient, sender)
        self._sender_age = age

    def greeting_msg(self):
        print("Happy birthday!")
        super().greeting_msg()
        print("The sender age is", self._sender_age)
