def divide_by_four(number):
    return number % 4 == 0


def four_dividers(number):
    return list(filter(divide_by_four, range(1, number)))


def main():
    print(four_dividers(9))
    print(four_dividers(3))


main()

