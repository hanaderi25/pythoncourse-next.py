def is_funny(string):
    return len([char for char in string if char != 'h' and char != 'a']) == 0


def main():
    print(is_funny("hahahahahaha"))
