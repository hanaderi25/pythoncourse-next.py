def main():
    length = int(input('Enter a name length: '))
    with open('UNIT 1/names.txt', 'r') as f:
        [print(name.strip()) for name in f if len(name.strip()) == length]


main()
