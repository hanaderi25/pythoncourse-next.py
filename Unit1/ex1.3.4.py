def find_password(password):
    return "".join([chr(ord(char) + 2) for char in password])


def main():
    password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
    print(find_password(password))
