def is_prime(number):
    return len([x for x in range(2, int(number / 2) + 1) if number % x == 0]) == 0


def main():
    print(is_prime(42))
    print(is_prime(43))


main()