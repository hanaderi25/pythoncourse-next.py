def double_char(char):
    return char + char


def double_letter(my_str):
    return "".join(map(double_char, my_str))


def main():
    print(double_letter("python"))
    print(double_letter("we are the champions!"))


main()