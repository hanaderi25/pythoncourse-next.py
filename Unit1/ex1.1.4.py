def sum_of_digits(number):
    return sum(map(int, str(number)))


def main():
    print(sum_of_digits(104))


main()
