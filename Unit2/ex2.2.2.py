class Dog:
    def __init__(self):
        self.name = "Rex"
        self.age = 1

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age



def main():
    first_dog = Dog()
    second_dog = Dog()
    second_dog.birthday()
    print("first dog age: " + str(first_dog.get_age()))
    print("second dog age: " + str(second_dog.get_age()))


main()
