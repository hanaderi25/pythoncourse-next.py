class Dog:

    count_animals = 0

    def __init__(self, name="Rex"):
        self._name = name
        self._age = 1
        Dog.count_animals += 1

    def birthday(self):
        self._age += 1

    def get_age(self):
        return self._age

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name


def main():
    first_dog = Dog("Snoop")
    second_dog = Dog()
    second_dog.birthday()
    print("first dog name: " + first_dog.get_name())
    print("second dog name: " + second_dog.get_name())
    first_dog.set_name("Snoopy")
    print("first dog name: " + first_dog.get_name())
    print("count animals:" + str(Dog.count_animals))


main()
