class Animal:
    zoo_name = "Hayaton"

    def __init__(self, name, hanger=0):
        self._hanger = hanger
        self._name = name

    def get_name(self):
        return self._name

    def is_hungry(self):
        if self._hanger > 1:
            return True
        else:
            return False

    def feed(self):
        self._hanger -= 1

    def talk(self):
        print("")


class Dog(Animal):

    def __init__(self, name, hanger=0):
        Animal.__init__(self, name, hanger)

    def talk(self):
        print("woof woof")

    def fetch_stick(self):
        print("There you go, sir!")


class Cat(Animal):

    def __init__(self, name, hanger=0):
        Animal.__init__(self, name, hanger)

    def talk(self):
        print("meow")

    def chase_laser(self):
        print("Meeeeow")

class Skunk(Animal):
    def __init__(self, name, hanger=0, stink_count=6):
        Animal.__init__(self, name, hanger)
        self._stink_count = stink_count

    def talk(self):
        print("tsssss")

    def stink(self):
        print("Dear lord!")

class Unicorn(Animal):
    def __init__(self, name, hanger=0):
        Animal.__init__(self, name, hanger)

    def talk(self):
        print("Good day, darling")

    def sing(self):
        print("I’m not your toy...")

class Dragon(Animal):
    def __init__(self, name, hanger=0, color="Green"):
        Animal.__init__(self, name, hanger)
        self._color = color

    def talk(self):
        print("Raaaawr")

    def breath_fire(self):
        print("$@#$#@$")

def main():
    first_dog = Dog("Browmie", 10)
    second_dog = Dog("Doggo", 80)
    first_cat = Cat("Zelda", 3)
    second_cat = Cat("Kitty", 80)
    first_skunk = Skunk("Stinky", 3)
    second_skunk = Skunk("Stinky Jr.", 80)
    first_unicorn = Unicorn("Keith", 7)
    second_unicorn = Unicorn("Clair", 80)
    first_dragon = Dragon("Lizzy", 1450)
    second_dragon = Dragon("McFly", 80)
    zoo_lst = [first_dog,second_dog, first_cat, second_cat, first_skunk, second_skunk,
               first_unicorn, second_unicorn, first_dragon, second_dragon]
    for animal in zoo_lst:
        print(type(animal).__name__ + " " + animal.get_name())
        while animal.is_hungry():
            animal.feed()
        animal.talk()

        if isinstance(animal, Dog):
            animal.fetch_stick()
        if isinstance(animal, Cat):
            animal.chase_laser()
        if isinstance(animal, Skunk):
            animal.stink()
        if isinstance(animal, Unicorn):
            animal.sing()
        if isinstance(animal, Dragon):
            animal.breath_fire()
    print(Animal.zoo_name)


main()