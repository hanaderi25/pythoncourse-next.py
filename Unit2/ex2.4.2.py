class BigThing:

    def __init__(self, value):
        self._value = value

    def size(self):
        if type(self._value) == int:
            return self._value
        else:
            return len(self._value)


class BigCat(BigThing):

    def __init__(self, name, weight):
        self._name = name
        self._weight = weight

    def size(self):
        if self._weight > 20:
            return "Very Fat"
        elif self._weight > 15:
            return "Fat"
        else:
            return "OK"


def main():
    my_thing = BigThing("balloon")
    print(my_thing.size())
    cutie = BigCat("mitzy", 22)
    print(cutie.size())


main()