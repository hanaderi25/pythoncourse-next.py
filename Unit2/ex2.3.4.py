class Pixel:
    """
    _x - x coordinate
    _y - y coordinate
    _red - a value between 0 and 255
    _green - a value between 0 and 255
    _blue - a value between 0 and 255
    """

    def __init__(self, x=0, y=0, red=0, blue=0, green=0):
        self._x = x
        self._y = y
        self._red = red
        self._blue = blue
        self._green = green

    def set_coords(self, x, y):
        self._x = x
        self._y = y

    def set_grayscale(self):
        avg = (self._red + self._blue + self._green)/3
        self._red = avg
        self._green = avg
        self._blue = avg

    def print_pixel_info(self):
        if self._blue == 0 and self._green == 0 and self._red != 0:
            print("X: " + str(self._x)+ ", Y: " + str(self._y) +
                  ", Color:(" + str(self._red) + ", " + str(self._green) + ", " + str(self._blue) + ")" + "Red")
        elif self._blue != 0 and self._green == 0 and self._red == 0:
            print("X: " + str(self._x)+ ", Y: " + str(self._y) +
                  ", Color:(" + str(self._red) + ", " + str(self._green) + ", " + str(self._blue) + ")" + "Blue")
        elif self._blue == 0 and self._green != 0 and self._red == 0:
            print("X: " + str(self._x)+ ", Y: " + str(self._y) +
                  ", Color:(" + str(self._red) + ", " + str(self._green) + ", " + str(self._blue) + ")" + "Green")
        else:
            print("X: " + str(self._x)+ ", Y: " + str(self._y) +
                  ", Color:(" + str(self._red) + ", " + str(self._green) + ", " + str(self._blue) + ")"")")

def main():
    p = Pixel(5, 6, 250)
    p.print_pixel_info()
    p.set_grayscale()
    p.print_pixel_info()

main()
